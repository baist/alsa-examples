  stream       : PLAYBACK
  access       : RW_INTERLEAVED
  format       : S16_LE
  subformat    : STD
  channels     : 1
  rate         : 44100
  exact rate   : 44100 (44100/1)
  msbits       : 16
  buffer_size  : 22050
  period_size  : 4410
  period_time  : 100000
  tstamp_mode  : NONE
  tstamp_type  : GETTIMEOFDAY
  period_step  : 1
  avail_min    : 4410
  period_event : 0
  start_threshold  : 22050
  stop_threshold   : 22050
  silence_threshold: 0
  silence_size : 0
  boundary     : 6206523236469964800


  stream - тип: воспроизведение или запись
  access - режим доступа
  format - формат данных
  subformat - 
  channels - каналы
  rate - частота дискретизации
  exact rate - 
  msbits - значимые биты
  buffer_size - размер буффера(фреймы)
  period_size - размер периода(фреймы)
  period_time - время периода(микросекунда)
  tstamp_mode - режим метки времени
  tstamp_type - тип метки времени
  period_step - 
  avail_min - минимум для пробуждения
  period_event -
  start_threshold - порог запуска 
  stop_threshold - порог остановки 
  silence_threshold -
  silence_size - размер заполнения тишиной
  boundary -

sample -> frame -> period -> buffer


snd_pcm_open

snd_pcm_hw_params_any
snd_pcm_hw_params_set_rate_resample
snd_pcm_hw_params_set_access
snd_pcm_hw_params_set_format
snd_pcm_hw_params_set_channels
snd_pcm_hw_params_set_rate_near
snd_pcm_hw_params_set_buffer_time_near
snd_pcm_hw_params_get_buffer_size
snd_pcm_hw_params_set_period_time_near
snd_pcm_hw_params_get_period_size
snd_pcm_hw_params
snd_pcm_sw_params_current
snd_pcm_sw_params_set_start_threshold
snd_pcm_sw_params_set_avail_min
snd_pcm_sw_params

snd_pcm_writei
snd_pcm_resume
snd_pcm_prepare

snd_pcm_close
